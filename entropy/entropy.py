import os

import numpy as np
import matplotlib.pyplot as plt


def read_bytes(filepath):
    """
    Reading bytes of the file
    :param filepath: path of the input file
    :return: bytes of the file
    """
    with open(filepath, mode='rb') as binary_file:
        bytes = binary_file.read()
    return bytes


def compute_probabilities(bytes_in=bytes()):
    """
    Computes (estimates) probabilities of bytes based on sample
    :param bytes_in: bytes distribution sample
    :return: numpy array of shape 256 with bytes probabilities
    """
    frequencies = np.zeros(shape=256)
    for b in bytes_in:
        frequencies[b] += 1
    return frequencies / len(bytes_in)


def compute_entropy(bytes_in=bytes()):
    """
    Computes Shannon entropy of bytes sample given
    :param bytes_in: bytes distribution sample
    :return: Shannon entropy
    """
    p = compute_probabilities(bytes_in)
    # we do not consider zero probabilities:
    # log2(0) will raise error,
    # log2(1)=0 will not affect the results
    p = np.where(p == 0, 1, p)
    entropy = - p.T.dot(np.log2(p))
    return entropy


def compute_file_entropy(filepaths):
    """
    Computes entropy of the files given
    :param filename: indicates files list
    :return: numpy array with Shannon entropy for each of the files
    """
    entropy = np.zeros(shape=len(filepaths))
    for i in range(len(filepaths)):
        file_bytes = read_bytes(filepaths[i])
        entropy[i] = compute_entropy(file_bytes)
    return entropy


def process_dataset(dirpath, plot=False):
    """
    Processes the entire dataset with all parts related to each of the formats
    :param dirpath: dataset directory path
    :param plot: indicates if plotting is enabled
    :return: list of formats and 2 dicts with averages and variances for each of the formats
    """
    print('\nProcessing dataset: {}\n'.format(dirpath) + '=' * 35)
    parts = os.listdir(dirpath)
    ave = dict()
    var = dict()

    for p in parts:
        print("Format: {}".format(p))
        # get file paths for the dataset part (for example for 'png' files)
        p_path = os.path.join(dirpath, p)
        files = os.listdir(p_path)
        filepaths = [os.path.join(p_path, f) for f in files]
        # compute
        entropy = compute_file_entropy(filepaths)
        average = np.mean(entropy)
        variance = np.var(entropy, ddof=1)
        # plot entropy
        if plot:
            plot_entropy(p, entropy, average)
        # display computed data
        print(('>>Average entropy    : {0:.5f} bits\n' +
               '>>Average efficiency : {1:.5f}\n' +
               '>>Variance           : {2:.5f}\n').
              format(average, average / 8, variance))
        # to export data
        ave[p] = average
        var[p] = variance
    print('=' * 35)
    return parts, ave, var


def plot_entropy(title, entropy, average):
    # helpers
    xmin, xmax = 0, len(entropy)
    x = np.arange(xmin, xmax)
    # plot entropy
    plt.bar(x, entropy)

    # plot difference of entropy and average value
    ave_np = np.full(shape=xmax - xmin, fill_value=average)
    diff_bot = np.minimum(entropy, ave_np)
    diff_up = np.maximum(entropy, ave_np)
    plt.bar(x, diff_up - diff_bot, bottom=diff_bot)

    # plot border line
    plt.axhline(average, xmin=xmin, xmax=xmax, color='r', label='Average = {0:.2f}'.format(average))

    # set params and show
    plt.title(title)
    plt.xlabel('File #')
    plt.ylabel('Entropy')
    plt.legend(loc=(0, 0))
    plt.show()


if __name__ == '__main__':
    titles, aves, vars = process_dataset('dataset', plot=True)
