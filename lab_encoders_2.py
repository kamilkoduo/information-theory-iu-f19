import math
import numpy as np

import lab_encoders


def get_cumulatives(symbols, probs):
    """
    Computes cumulative probabilities
    :param symbols: list of alphabet symbols
    :param probs: symbols probabilities dict
    :return: cumulatives list
    """
    n = len(symbols)
    cumulatives = np.zeros(shape=n)
    cumulatives[0] = probs[symbols[0]]
    for i in range(1, n):
        cumulatives[i] = cumulatives[i - 1] + probs[symbols[i]]
    return cumulatives


def shannon_fano_elias_encoder(symbols, probs):
    """
    Shannon Fano Elias encoder
    :param symbols: list of alphabet symbols
    :param probs: symbols probabilities dict
    :return: codes dict
    """

    def code(x, n):
        assert (0 <= x <= 1)
        z = math.floor(x * (1 << n))
        encoded = "{0:b}".format(z).zfill(n)
        return encoded

    n = len(symbols)
    # compute cumulative probabilities
    cumulatives = get_cumulatives(symbols, probs)
    # compute F bar values
    half_pi = [probs[symbols[i]] / 2 for i in range(n)]
    f_bars = [cumulatives[i] - half_pi[i] for i in range(n)]
    # compute lengths
    lengths = [math.ceil(-math.log2(probs[symbols[i]])) + 1 for i in range(n)]
    # compute codes
    sfe_dict = dict()
    for i in range(len(symbols)):
        sfe_dict[symbols[i]] = code(f_bars[i], lengths[i])

    return sfe_dict


def shannon_fano_elias_decoder_single(symbols, probs, code):
    """
    Method follows algorithm from lab slides
    :param symbols: list of alphabet symbols
    :param probs: symbols probabilities dict
    :param code: single symbol code
    :return: decoded symbol
    """
    cumulatives = get_cumulatives(symbols, probs)

    low_decimal = int(code, 2)
    high_decimal = (low_decimal + 1)

    low_decimal /= 1 << len(code)
    high_decimal /= 1 << len(code)

    if high_decimal <= cumulatives[0]:
        return symbols[0]
    for i in range(1, len(symbols)):
        if cumulatives[i - 1] <= low_decimal and high_decimal < cumulatives[i]:
            return symbols[i]
    return None


def get_index_sequence(symbols, message):
    """
    Converts symbolic message to sequence of corresponding indexes in alphabet
    :param symbols: list of alphabet symbols
    :param message: symbolic message
    :return: index sequence
    """
    indexes = {symbols[i]: i for i in range(len(symbols))}
    seq = [indexes[m] for m in message]
    return seq


def get_seq_interval(seq, cumulatives):
    """
    Method computes interval for a sequence of symbols for arithmetic encoder
    :param seq: sequence of symbols
    :param cumulatives: cummulative probabilities c[i]=sum[p[i] for i =0..i]
    :return: low and high of the resulting interval
    """
    low = 0
    high = 1
    for i in seq:
        c_low = 0 if i == 0 else cumulatives[i - 1]
        c_high = cumulatives[i]
        new_low = c_low * (high - low) + low
        new_high = c_high * (high - low) + low
        low = new_low
        high = new_high
    return low, high


def get_shortest_binary(low, high):
    """
    computes the shortest binary representation of a number from the given interval
    :param low: interval low
    :param high: interval high
    :return: binary representation string of resulting number
    """
    num, a, i = 0, 0.5, 0
    while num < low:
        if num + a < high:
            num += a
        a /= 2
        i += 1
    num = math.floor(num * (1 << i))
    #print(low, high)
    #print("{0:b}".format(num).zfill(i))
    return "{0:b}".format(num).zfill(i)


def arithmetic_encoder(symbols, probs, message):
    """
    Arithmetic encoder
    :param symbols: list of alphabet symbols
    :param probs: symbols probabilities dict
    :param message: symbolic message to encode
    :return: encoded message
    """
    cumulatives = get_cumulatives(symbols, probs)
    seq = get_index_sequence(symbols, message)
    low, high = get_seq_interval(seq, cumulatives)
    bin = get_shortest_binary(low, high)
    return bin


def get_interval(value, intervals, low):
    """
    Finds index of the interval where value lies in
    intervals are [low,intervals[0]),...[intervals[k],intervals[k+1])
    :param value: to find interval of
    :param intervals: defined
    :param low: defined
    :return: index of the higher bound of the interval
    """
    if low <= value < intervals[0]:
        return 0
    for i in range(1, len(intervals)):
        if value < intervals[i]:
            return i
    return None


def arithmetic_decoder(symbols, probs, code, n):
    """
    Arithmetic decoder
    :param symbols: list of alphabet symbols
    :param probs: symbols probabilities dict
    :param code: encoded message
    :param n: number of symbols to encode
    :return: encoded message
    """
    cumulatives = get_cumulatives(symbols, probs)
    # numeric representation of the code
    num = int(code, 2) / (1 << len(code))
    seq = ''
    intervals = cumulatives
    low = 0
    for i in range(n):
        k = get_interval(num, intervals, low)
        seq += symbols[k]
        max = intervals[k]
        if k > 0:
            low = intervals[k - 1]
        intervals = (max - low) * cumulatives + low
    return seq


def prefix_code_decoder(codes, message, decoder_single, symbols, probs):
    """
    General prefix code decoder
    :param codes: codes dict
    :param message: encoded message to decode
    :param decoder_single: single symbol decoder function
    :param symbols: list of alphabet symbols - decoder_single argument
    :param probs: symbols probabilities dict - decoder_single argument
    :return: decoded message
    """
    decoded = ''
    c = ''
    i = 0
    codes_reversed = {codes[key]: key for key in codes}
    while i < len(message):
        while c not in codes_reversed:
            c += message[i]
            i += 1
        decoded += decoder_single(symbols, probs, c)
        c = ''
        i += 1
    return decoded


def compute_probabilities(message):
    probs = dict()
    for m in message:
        if m in probs:
            probs[m] += 1
        else:
            probs[m] = 0
    return probs


def test_sfe_encoder(message, p=None):
    if p is None:
        probs = compute_probabilities(message)
    else:
        probs = p
    symbols = list(probs.keys())
    codes = shannon_fano_elias_encoder(symbols, probs)
    encoded = ''
    for m in message:
        encoded += codes[m]

    print("TEST Shannon-Fano-Elias encoder")
    print("Message decoded: ", message)
    print("Encoded: ", encoded)
    print("Code length: ", len(encoded))
    print()


def test_sfe_decoder(message, probs):
    symbols = list(probs.keys())
    codes = shannon_fano_elias_encoder(symbols, probs)
    decoded = prefix_code_decoder(codes, message, shannon_fano_elias_decoder_single, symbols, probs)

    print("TEST Shannon-Fano-Elias decoder")
    print("Message encoded: ", message)
    print("Decoded: ", decoded)
    print()


def test_arithmetic_encoder(message, p):
    if p is None:
        probs = compute_probabilities(message)
    else:
        probs = p
    symbols = list(probs.keys())
    print('symbols ',symbols)
    symbols.reverse()
    print('reversed', symbols)
    encoded = arithmetic_encoder(symbols, probs, message)

    print("TEST Arithmetic encoder")
    print("Message decoded: ", message)
    print("Encoded: ", encoded)
    print("Code length: ", len(encoded))
    print()


def test_arithmetic_decoder(message, n, probs):
    symbols = list(probs.keys())
    decoded = arithmetic_decoder(symbols, probs, message, n)

    print("TEST Arithmetic decoder")
    print("Message encoded: ", message)
    print("Decoded: ", decoded)
    print("Degree: ", n)
    print()


if __name__ == '__main__':
    test_sfe_encoder("345", p={'1': 0.25, '2': 0.25, '3': 0.2, '4': 0.15, '5': 0.15})
    test_sfe_decoder("0001", probs={'1': 2 / 9, '2': 1 / 9, '3': 1 / 3, '4': 1 / 3})
    test_arithmetic_encoder("iou", p={'a': 0.12, 'e': 0.42, 'i': 0.09, 'o': 0.3, 'u': 0.07})
    test_arithmetic_decoder("1001", 1, probs={'a': 0.12, 'e': 0.42, 'i': 0.09, 'o': 0.3, 'u': 0.07})


    # Huffman
    hd = lab_encoders.huffman_coding({'a': 12, 'e': 42, 'i': 9, 'o': 30, 'u': 7})
    encoded = ''
    message = 'iou'
    for i in message:
        encoded += hd[i]

    print("TEST Huffman encoder")
    print("Message decoded: ", message)
    print("Encoded: ", encoded)
    print("Code length: ", len(encoded))
    print()

