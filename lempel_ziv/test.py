import sys

import lempel_ziv.lempel_ziv


def validate(filename1: str, filename2: str):
    try:
        with open(filename1, 'rb') as file1:
            bytes1 = file1.read()
            with open(filename2, 'rb') as file2:
                bytes2 = file2.read()
                for i in range(len(bytes1)):
                        if bytes1[i] != bytes2[i]:
                            print(i)
                            return False
                return True
    except FileNotFoundError as error:
        return error


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Not enought files to compare: expect 2')
    elif len(sys.argv) > 3:
        print('Too much files to compare: expect 2')
    else:
        print(validate(sys.argv[1], sys.argv[2]))
