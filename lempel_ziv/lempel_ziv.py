import os
import shutil
import numpy as np
import matplotlib.pyplot as plt


def _read_bytes(filepath):
    """
    Reading bytes of the file
    :param filepath: path of the input file
    :return: bytes of the file
    """
    with open(filepath, mode='rb') as binary_file:
        bytes_in = binary_file.read()
    return bytes_in


def __bin_to_bytes(bin_in):
    """
    Converts binary string to bytes
    it splits the input string to chunks 8 symbols each (1 byte)
    :param bin_in: input string contains 0s and 1s
    :return: returns converted bytes from bin string
    """
    chunks = [bin_in[i:i + 8] for i in range(0, len(bin_in), 8)]
    b_chunks = b''
    for c in chunks:
        t = int(c, 2).to_bytes(length=1, byteorder='big')
        b_chunks += t
    return b_chunks


def __bytes_to_bin(bytes_in):
    """
    Converts bytes to binary string
    :param bytes_in: input string contains 0s and 1s
    :return: returns converted bytes to bin string
    """
    s = []
    for i in range(len(bytes_in)):
        byte = bin(bytes_in[i])[2:]
        if len(byte) % 8 != 0:
            byte = '0' * (8 - len(byte) % 8) + byte
        s.append(byte)
    s = ''.join(s)
    return s


def __encode_index(ind):
    """
    Encodes integer index to bytes string considering signal bits in bytes
    signal 1 in the last byte of the index, 0s in others
    :param ind: integer index
    :return: bytes string
    """
    encoded = b''
    encoded += (128 + (ind % 128)).to_bytes(length=1, byteorder='big')
    ind //= 128

    while ind > 0:
        encoded += (ind % 128).to_bytes(length=1, byteorder='big')
        ind //= 128
    return encoded[::-1]


def __decode_index(ind_bytes):
    s = __bytes_to_bin(ind_bytes)
    s = list(s)
    s.reverse()
    for i in range(7, len(s), 7):
        s = s[:i] + s[i + 1:]
    if len(s) % 8 != 0:
        s += (8 - len(s) % 8) * ['0']
    s.reverse()
    s = ''.join(s)
    while s[:8] == '0' * 8 and len(s) > 8:
        s = s[8:]
    return int(s, 2)


def __build_sequences_enc(data):
    """
    Reads the given byte sequence

    Builds up the tree of Lempel Ziv compression algorithm
    Tree node for each subsequence represents its:
    location, inherited location (ancestor), byte appended and corresponding encoding of the subsequence

    Builds up the list of subsequences to encode
    List consists of all subsequences read from the input byt sequence

    :param data: byte sequence to encode
    :return: Tree, List
    """
    s = b''
    seqs = [b'']
    tree = dict()
    i = 0
    tree[b''] = (0, 0, b'', b'')
    while i < len(data):
        byte = data[i].to_bytes(length=1, byteorder='big')
        word = s + byte
        if word not in tree:
            inh_loc = tree[s][0]
            code = __encode_index(inh_loc) + byte
            tree[word] = (len(seqs), inh_loc, byte, code)
            s = b''
            seqs.append(word)
        else:
            s = word
        i += 1
    if s != b'':
        seqs.append(s)
    del tree[b'']
    del seqs[0]
    return seqs, tree


def __build_sequences_dec(data):
    index = b''
    seqs = [b'']
    tree = dict()
    tree[b''] = (0, 0, b'', b'')
    i = 0
    while i < len(data):
        index += data[i].to_bytes(length=1, byteorder='big')
        if data[i] & 128 != 0:
            byte = data[i + 1].to_bytes(length=1, byteorder='big')
            dec_index = __decode_index(index)
            code = index + byte
            word = tree[seqs[dec_index]][3] + byte
            tree[code] = (len(seqs), index, byte, word)
            seqs.append(code)
            i += 2
            index = b''
        else:
            i += 1
    del tree[b'']
    del seqs[0]
    return seqs, tree


def __encode_bytes(data, target_path):
    """
    Encodes given byte sequence according to Lempel Ziv compression algorithm and writes the results to the file
    First, build the subsequence list and tree
    Then, encode the each element of the list according to the code stored in the tree
    and write results to the file
    :param data: input bytes sequence
    :param target_path: path of the output file with results
    :return:
    """
    seqs, tree = __build_sequences_enc(data)
    with open(target_path, mode='wb') as binary_file:
        for s in seqs:
            c = tree[s][3]
            binary_file.write(c)
        binary_file.flush()
    return


def __decode_bytes(data, target_path):
    """
    Decodes given byte sequence according to Lempel Ziv decompression algorithm and writes the results to the file
    First, build the subsequence list and tree
    Then, decode the each element of the list according to the codes stored in the tree
    and write results to the file
    :param data: input bytes sequence
    :param target_path: path of the output file with results
    :return:
    """
    seqs, tree = __build_sequences_dec(data)
    with open(target_path, mode='wb') as binary_file:
        for s in seqs:
            c = tree[s][3]
            binary_file.write(c)
        binary_file.flush()
    return


def _encode_binary_file(source_path, target_path):
    """
    Given the input and output file paths it encodes the source and stores the results
    :param source_path: source file to encode
    :param target_path: results file with encoded bytes
    :return:
    """
    bs = _read_bytes(source_path)
    __encode_bytes(bs, target_path)
    return


def _decode_binary_file(source_path, target_path):
    """
    Given the input and output file paths it decodes the source and stores the results
    :param source_path: source file to decode
    :param target_path: results file with decoded bytes
    :return:
    """
    bs = _read_bytes(source_path)
    __decode_bytes(bs, target_path)
    return


def _get_compression_file_paths(source_path, target_path):
    source_file_names = os.listdir(source_path)
    source_file_paths = [os.path.join(source_path, fn) for fn in source_file_names]

    name_ext = [(''.join(fn.split('.')[:-1]), fn.split('.')[-1]) for fn in source_file_names]

    compression_file_names = ['{}Compressed.{}'.format(n, e) for n, e in name_ext]
    compression_file_paths = [os.path.join(target_path, fn) for fn in compression_file_names]

    if not os.path.exists(target_path):
        os.makedirs(target_path)

    return source_file_paths, compression_file_paths


def _get_decompression_file_paths(source_path, target_path):
    source_file_names = os.listdir(source_path)
    source_file_paths = [os.path.join(source_path, fn) for fn in source_file_names]

    name_ext = [(''.join(fn.split('.')[:-1]), fn.split('.')[-1]) for fn in source_file_names]
    i = 0
    while i < len(name_ext):
        if not name_ext[i][0].endswith('Compressed'):
            del name_ext[i]
        else:
            i += 1
    print(name_ext)

    decompression_file_names = ['{}Decompressed.{}'.format(n[:-10], e) for n, e in name_ext]
    decompression_file_paths = [os.path.join(target_path, f) for f in decompression_file_names]

    if not os.path.exists(target_path):
        os.makedirs(target_path)

    return source_file_paths, decompression_file_paths


def _report_progress(i, n, s='complete'):
    """
    To print progress bar
    :param i: steps done
    :param n: steps total
    :return: returns current progress
    """
    k = i / n
    k = int(k * 100)
    progress_line = 'Progress: |{:<100}| {:>3}% {}'.format('=' * k, k, s)
    print('\r', end='', flush=True)
    print(progress_line, end='', flush=True)

    return k


def compress_dataset(source_path, target_path):
    print('>Compression of dataset \'{}\' into \'{}\''.format(source_path, target_path))
    source_parts = os.listdir(source_path)
    for s in source_parts:
        print('>>Processing part: \'{}\''.format(s))
        source_part_path = os.path.join(source_path, s)
        target_part_path = os.path.join(target_path, s)
        s_paths, t_paths = _get_compression_file_paths(source_part_path, target_part_path)

        n = len(s_paths)
        for i in range(n):
            _report_progress(i + 1, n, 'files compressed! ')
            _encode_binary_file(s_paths[i], t_paths[i])
        print()
    return


def decompress_dataset(source_path, target_path):
    print('>Decompression of dataset \'{}\' into \'{}\''.format(source_path, target_path))
    source_parts = os.listdir(source_path)
    for s in source_parts:
        print('>>Processing part: \'{}\''.format(s))
        source_part_path = os.path.join(source_path, s)
        target_part_path = os.path.join(target_path, s)
        s_paths, t_paths = _get_decompression_file_paths(source_part_path, target_part_path)

        n = len(s_paths)
        for i in range(n):
            _report_progress(i + 1, n, 'files decompressed! ')
            _decode_binary_file(s_paths[i], t_paths[i])
        print()
    return


def process_dataset(source_path, target_path):
    if os.path.exists(target_path):
        shutil.rmtree(target_path)
    compress_dataset(source_path, target_path)
    decompress_dataset(target_path, target_path)


def plot(title, values, average, ylabel, xlabel, tick_labels=None, tick_rotation=None):
    # helpers
    xmin, xmax = 0, len(values)
    x = np.arange(xmin, xmax)
    # plot entropy
    plt.bar(x, values)

    # plot difference of entropy and average value
    ave_np = np.full(shape=xmax - xmin, fill_value=average)
    diff_bot = np.minimum(values, ave_np)
    diff_up = np.maximum(values, ave_np)
    plt.bar(x, diff_up - diff_bot, bottom=diff_bot)
    if tick_labels is not None:
        plt.xticks(ticks=x, labels=tick_labels, rotation=tick_rotation, ha='right', rotation_mode='anchor')

    # plot border line
    plt.axhline(average, xmin=xmin, xmax=xmax, color='r', label='Average = {0:.2f}'.format(average))

    # set params and show
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend(loc=(0, 0))
    plt.show()


def plot_types():
    o_parts = os.listdir('dataset')

    for o in o_parts:
        source_part_path = os.path.join('dataset', o)
        target_part_path = os.path.join('KamilAhmetovOutputs', o)
        s_paths, t_paths = _get_compression_file_paths(source_part_path, target_part_path)
        s_s = [os.path.getsize(s) for s in s_paths]
        t_s = [os.path.getsize(s) for s in t_paths]
        r = [s / t for s, t in zip(s_s, t_s)]
        av = sum(r) / len(r)

        plot(o, r, av, ylabel="Compression Ratio", xlabel="File #")


def plot_by_type():
    o_parts = os.listdir('dataset')

    labels = []
    values = []
    for o in o_parts:
        source_part_path = os.path.join('dataset', o)
        target_part_path = os.path.join('KamilAhmetovOutputs', o)
        s_paths, t_paths = _get_compression_file_paths(source_part_path, target_part_path)
        n = len(s_paths)
        s_s = sum([os.path.getsize(s) for s in s_paths]) / n
        t_s = sum([os.path.getsize(s) for s in t_paths]) / n
        r = s_s / t_s
        labels.append(o)
        values.append(r)

    ave = sum(values) / len(values)
    plot(title="By types", values=values, average=ave, tick_labels=labels, xlabel="Type", ylabel="Compression Ratio")


def plot_by_type_size(bins=15):
    o_parts = os.listdir('dataset')

    for o in o_parts:
        source_part_path = os.path.join('dataset', o)
        target_part_path = os.path.join('KamilAhmetovOutputs', o)
        s_paths, t_paths = _get_compression_file_paths(source_part_path, target_part_path)
        s_s = [os.path.getsize(s) for s in s_paths]
        t_s = [os.path.getsize(s) for s in t_paths]

        m = max(s_s)
        thresholds = [m * i / bins for i in range(bins)] + [m + 1]
        print(thresholds)

        r = []
        l = []
        for i in range(bins):
            r.append([])
            for s, t in zip(s_s, t_s):
                if thresholds[i] <= s < thresholds[i + 1]:
                    r[i].append(s / t)
            l.append(len(r[i]))
            if len(r[i]) == 0:
                r[i] = 0
            else:
                r[i] = sum(r[i]) / len(r[i])

        print()
        print(r)
        print(l)
        print(thresholds)
        av = sum(r) / len(r)
        labels = ['{} in range [{}KB, {}KB)'.format(l[i], int(thresholds[i] // 1000), int(thresholds[i + 1] // 1000))
                  for i in range(bins)]
        plot(title=o + " by sizes", values=r, average=av, ylabel="Compression ratio", xlabel="File Size",
             tick_labels=labels, tick_rotation=45)


def plot_main():
    plot_types()
    plot_by_type()
    plot_by_type_size()


if __name__ == '__main__':
    process_dataset(source_path='dataset', target_path='KamilAhmetovOutputs')
