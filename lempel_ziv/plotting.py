import os

import numpy as np
import matplotlib.pyplot as plt

import lempel_ziv


def plot(title, values, average, ylabel, xlabel, tick_labels=None, tick_rotation = None):
    # helpers
    xmin, xmax = 0, len(values)
    x = np.arange(xmin, xmax)
    # plot entropy
    plt.bar(x, values)

    # plot difference of entropy and average value
    ave_np = np.full(shape=xmax - xmin, fill_value=average)
    diff_bot = np.minimum(values, ave_np)
    diff_up = np.maximum(values, ave_np)
    plt.bar(x, diff_up - diff_bot, bottom=diff_bot)
    if tick_labels is not None:
        plt.xticks(ticks=x, labels=tick_labels, rotation = tick_rotation, ha='right',rotation_mode='anchor')

    # plot border line
    plt.axhline(average, xmin=xmin, xmax=xmax, color='r', label='Average = {0:.2f}'.format(average))

    # set params and show
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend(loc=(0, 0))
    plt.show()


def plot_types():
    o_parts = os.listdir('dataset')

    for o in o_parts:
        source_part_path = os.path.join('dataset', o)
        target_part_path = os.path.join('KamilAhmetovOutputs', o)
        s_paths, t_paths = lempel_ziv._get_compression_file_paths(source_part_path, target_part_path)
        s_s = [os.path.getsize(s) for s in s_paths]
        t_s = [os.path.getsize(s) for s in t_paths]
        r = [s / t for s, t in zip(s_s, t_s)]
        av = sum(r) / len(r)

        plot(o, r, av, ylabel="Compression Ratio", xlabel="File #")


def plot_by_type():
    o_parts = os.listdir('dataset')

    labels = []
    values = []
    for o in o_parts:
        source_part_path = os.path.join('dataset', o)
        target_part_path = os.path.join('KamilAhmetovOutputs', o)
        s_paths, t_paths = lempel_ziv._get_compression_file_paths(source_part_path, target_part_path)
        n = len(s_paths)
        s_s = sum([os.path.getsize(s) for s in s_paths]) / n
        t_s = sum([os.path.getsize(s) for s in t_paths]) / n
        r = s_s / t_s
        labels.append(o)
        values.append(r)

    ave = sum(values) / len(values)
    plot(title="By types", values=values, average=ave, tick_labels=labels, xlabel="Type", ylabel="Compression Ratio")


def plot_by_type_size(bins = 15):
    o_parts = os.listdir('dataset')

    for o in o_parts:
        source_part_path = os.path.join('dataset', o)
        target_part_path = os.path.join('KamilAhmetovOutputs', o)
        s_paths, t_paths = lempel_ziv._get_compression_file_paths(source_part_path, target_part_path)
        s_s = [os.path.getsize(s) for s in s_paths]
        t_s = [os.path.getsize(s) for s in t_paths]

        m = max(s_s)
        thresholds = [m * i / bins for i in range(bins)] + [m + 1]
        print(thresholds)

        r = []
        l=[]
        for i in range(bins):
            r.append([])
            for s, t in zip(s_s, t_s):
                if s >= thresholds[i] and s < thresholds[i + 1]:
                    r[i].append(s / t)
            l.append(len(r[i]))
            if len(r[i]) ==0:
                r[i]=0
            else:
                r[i] = sum(r[i]) / len(r[i])

        print()
        print(r)
        print(l)
        print(thresholds)
        av = sum(r) / len(r)
        labels = ['{} in range [{}KB, {}KB)'.format(l[i], int(thresholds[i]//1000), int(thresholds[i+1]//1000)) for i in range(bins)]
        plot(title=o+" by sizes", values=r, average=av, ylabel="Compression ratio", xlabel="File Size", tick_labels=labels, tick_rotation=45)
        plot(title=o + " by sizes", values=r, average=av, ylabel="Compression ratio", xlabel="File Size",
             tick_labels=labels, tick_rotation=45)


def main():
    # plot_types()
    # plot_by_type()
    plot_by_type_size()


if __name__=='__main__':
    main()