from __future__ import print_function
from operator import itemgetter


def shannon_coding(freqs):
    def shannon_coding_req(sequence, code):
        total = sum(sequence.values())
        left = {}
        right = {}
        if len(sequence) == 1:
            shannon_dict[sequence.popitem()[0]] = code
            return
        flag = False
        for i in sorted(sequence.items(), key=itemgetter(1), reverse=True):
            if not flag and 2 * sum(left.values()) + sequence[i[0]] < total:
                left[i[0]] = sequence[i[0]]
            else:
                flag = True
                right[i[0]] = sequence[i[0]]
        shannon_coding_req(left, code + "0")
        shannon_coding_req(right, code + "1")

    shannon_dict = {}
    shannon_coding_req(freqs, "")
    return shannon_dict


def huffman_coding(sequence):
    huffman_dict = dict.fromkeys(sequence.keys(), "")
    tree = []
    for i in sequence.items():
        tree.append([i[1], [i[0]]])
    while len(tree) != 1:
        tree.sort()
        for i in tree[0][1]:
            huffman_dict[i] += "0"
        for i in tree[1][1]:
            huffman_dict[i] += "1"
        tree[1][0] += tree[0][0]
        tree[1][1].extend(tree[0][1])
        tree.pop(0)
    return huffman_dict


def process(message, encoder):
    print('\n' + '=' * 200)
    print('Processing message using encoder {}'.format(encoder.__name__))
    print('Message: {}'.format(message))

    # count frequencies
    freq = {}
    for c in message:
        if c not in freq:
            freq[c] = 0
        freq[c] += 1

    for c in sorted(freq):
        print('{} => {} times. Probability: {}'.format(c, freq[c], freq[c] / len(message)))

    # encodings
    print("Encodings:")
    code_dict = encoder(freq)
    for i in sorted(code_dict):
        print("{} = {}".format(i, code_dict[i]))

    # encoded message
    code_mes = ""
    for i in message:
        code_mes += code_dict[i]
    print("Encoded message length: {}\nEncoded message: {}".format(len(code_mes), code_mes))

    # expected length
    exp_length = 0
    for i in sorted(code_dict):
        exp_length += len(code_dict[i]) * freq[i] / len(message)
    print("Expected length of code: {}".format(exp_length))
    print('=' * 200 + '\n')


if __name__ == '__main__':
    message = ['a'] * 24 + ['b'] * 12 + ['c'] * 10 + ['d'] * 8 + ['e'] * 8  # + ['f'] * 1 + ['g'] * 2
    # message = "abccccddc"
    # message = 'aaaabbbccd'

    process(message, shannon_coding)
    process(message, huffman_coding)
