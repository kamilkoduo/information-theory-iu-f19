import os

from Crypto.Cipher import AES, DES3
from Crypto.Random import get_random_bytes

AES_KEY = get_random_bytes(16)

DES_KEY = get_random_bytes(24)
DES_KEY = DES3.adjust_key_parity(DES_KEY)


def save(bytes, name):
    with open(name, 'wb') as file:
        file.write(bytes)


def main():
    file_name = input("Enter file name: ")
    with open(file_name, 'rb') as input_file:
        input_file.seek(0, os.SEEK_END)
        size = input_file.tell()
        input_file.seek(0, os.SEEK_SET)
        print("File size = ", size)
        input_bytes = input_file.read(size)

        # AES
        aes_cipher = AES.new(AES_KEY, AES.MODE_EAX)
        aes_nonce = aes_cipher.nonce
        aes_enc = aes_cipher.encrypt(input_bytes)

        aes_cipher = AES.new(AES_KEY, AES.MODE_EAX, nonce=aes_nonce)
        aes_dec = aes_cipher.decrypt(aes_enc)

        save(aes_enc, "aes.enc." + file_name)
        save(aes_dec, "aes.dec." + file_name)

        # DES
        des_cipher = DES3.new(DES_KEY, DES3.MODE_EAX)
        des_nonce = des_cipher.nonce
        des_enc = des_cipher.encrypt(input_bytes)
        des_cipher = DES3.new(DES_KEY, DES3.MODE_EAX, nonce=des_nonce)
        des_dec = des_cipher.decrypt(des_enc)

        save(des_enc, "des.enc." + file_name)
        save(des_dec, "des.dec." + file_name)


if __name__ == '__main__':
    main()
